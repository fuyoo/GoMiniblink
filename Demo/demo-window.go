package main

import (
	"fmt"
	gm "gitee.com/fuyoo/GoMiniblink"
	fm "gitee.com/fuyoo/GoMiniblink/forms"
	cs "gitee.com/fuyoo/GoMiniblink/forms/controls"
	"gitee.com/fuyoo/GoMiniblink/forms/windows"
	"unsafe"
)

func main() {
	fmt.Println("x64 is", unsafe.Sizeof(uintptr(0)) == 8)
	cs.App = new(windows.Provider).Init()
	cs.App.SetIcon("app.ico")

	frm := new(gm.MiniblinkForm).Init()
	frm.SetTitle("miniblink窗口")
	frm.SetLocation(100, 100)
	frm.SetSize(800, 500)
	frm.SetBorderStyle(fm.FormBorder_None)
	frm.NoneBorderResize()
	frm.View.ResourceLoader = append(frm.View.ResourceLoader, new(gm.FileLoader).Init("Res", "local"))
	frm.EvLoad["show"] = func(s cs.GUI) {
		frm.View.LoadUri("http://local/window.html")
	}
	cs.Run(&frm.Form)
}
